Before do
  @base_url = nil
  @accept_next_alert = true
  @verification_errors = []

  #@browser = Selenium::Browser.new :firefox
  @browser = Watir::Browser.new :firefox
  @browser.window.maximize

end

After do |scenario|
  if (scenario.failed?)
    #@browser.screenshot.save("./reports/screenshots/Erro_"+Time.now.utc.iso8601+"_screen_error.png")
    #Watir
    encoded_img = @browser.screenshot.base64
    #Selenium
    #encoded_img = @browser.screenshot_as(:base64)
    #set in report
    embed("data:image/png;base64,#{encoded_img}", 'image/png')
  end

  @browser.close

end

