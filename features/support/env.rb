require 'watir-webdriver'
require 'selenium-webdriver'
require 'page-object'
require 'rspec'
require 'active_record'
require 'data_magic'
require 'time'
require 'rubygems'
#require 'watir-scroll'
require 'rautomation'

World(PageObject::PageFactory)
World(RSpec::Matchers)
DataMagic.yml_directory = 'config/data'


