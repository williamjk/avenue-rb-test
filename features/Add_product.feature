Feature: Add a product to cart
  As a customer
  I want to go to a product page and add the product to the cart
  I should be able to see this product added to cart on shopping cart page

  Scenario: Add product product in cart, go and assert checkout page
    Given i stay on the ecommerce page "http://www.williams-sonoma.com/".
    And access category "Cookware" in menu.
    And access subcategory "Cookware Sets" in menu.
    And click on product "product".
    Then Product page shows Add to cart button.
    And Add product "Williams-Sonoma Stainless Steel Thermo-Clad™ 15-Piece Cookware Set" to cart.
    When click on Add to cart button, add to cart overlay appears.
    Then The product "Williams-Sonoma Stainless Steel Thermo-Clad™ 15-Piece Cookware Set" you added to cart should be on shopping cart page.
    When Checkout button is on the add to cart overlay.
    When click on Checkout button.
    Then shopping cart page is shown.