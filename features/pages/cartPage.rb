
class CartPage

  include PageObject
  include RSpec::Matchers

  h1(:cartTitle, :xpath => '//*[@id="cartForm"]/div[1]/h1')

  def assertPageIsCartPage
    self.cartTitle_element == "Shopping Cart"
  end

end