
class SubCategoryPage

  include PageObject
  include RSpec::Matchers

  link :product, :xpath => '//*[@id="subCatListContainer"]/ul/li[1]/a'

  def clickOnProduct
    self.product?
    self.product
  end

end