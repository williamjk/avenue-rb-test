
class CartOverlayPage

  include PageObject
  include RSpec::Matchers

  link :btnCheckout, :href => 'http://www.williams-sonoma.com/checkout/normal.html'
  h4(:cartOverlapsTitle, :id => 'title')
  h1(:prodName, :xpath => '//*[@id="purchasing-container"]/div[2]/div/h1')
  span(:prodPrice, :xpath => '//*[@id="itemselection"]/div/section/section/div/div/div/div[2]/span/span[1]/span[1]/span[2]')
  h1(:pageTitle, :class => 'overlayTitle')

  def assertCheckoutButtonIsPresented
    self.btnCheckout?
  end

  def assertProductOnCart
    self.cartOverlapsTitle_element == "Williams-Sonoma Stainless-Steel Thermo-Clad 15-Piece Cookware Set"
  end

  def clickCheckoutButton
    self.btnCheckout
  end

  def assertNameAndPriceInCartOverlay name, price
    sleep 3
    self.prodName?
    self.prodName == name
    self.prodPrice == price
  end

  def assertCartOverlayPageExists
    pageTitle == "You've just added the following to your basket:"
  end

end
