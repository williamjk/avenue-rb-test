
class ProductPage

  include PageObject
  include RSpec::Matchers

  button :btnAddToCart, :class => 'btn btn_addtobasket btn_addtobasket_add'

  def assertProductPageShowAddToCardButton
    self.btnAddToCart?
  end

  def addProductToCard
    self.btnAddToCart
  end

end