class ResultSearchPage

  include PageObject
  include RSpec::Matchers

  h3(:textResultForSearch, :xpath => '//*[@id="results-summary"]/h3')
  span(:quickView, :class => 'quicklook-link')
  link(:productNameClicked, :xpath => '//*[@id="content"]/div[2]/ul/li[1]/a')
  span(:productPriceClicked, :class => 'price-amount')


  def assertIsSearchResultPage
    self.textResultForSearch?
  end

  def scrollToQuickView
    self.quickView_element.hover

    # This work only using Selenium WeDriver
    #driver.action.move_to(element).perform
    #driver.mouse.move_to(element)

    # This is a possible option with Watir but don't work for my
    " $position = quickView.wd.location
   posX = $position[0]
   posY = $position[1]

    mouse = @browser.rautomation.mouse
    puts mouse.position.inspect
    mouse.move :x => posX, :y => posY
    puts mouse.position.inspect"
  end

  def storeProductNameClicked
    self.productNameClicked_element.exists?
    return productNameClicked_element.text
  end

  def storeProductPriceClicked
    self.productPriceClicked_element.exists?
    return productPriceClicked_element.text
  end

  def clickQuickOverview
    @browser.element(:xpath => '//*[@id="content"]/div[2]/ul/li[1]/div[1]/a[2]').click
    #quickView
  end

end