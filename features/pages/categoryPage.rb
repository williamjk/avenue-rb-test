
class CategoryPage

  include PageObject
  include RSpec::Matchers

  link :itemSubMenuCookware, :xpath => '//*[@id="super-category"]/div[1]/div[1]/ul[2]/li[1]/a'

  def clickSubCategory
    self.itemSubMenuCookware?
    self.itemSubMenuCookware

  end

end