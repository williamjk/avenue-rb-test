class IndexPage

  include PageObject
  include RSpec::Matchers

  page_url("http://www.williams-sonoma.com/")

  link :itemMenuCookware, :xpath => '//*[@id="topnav-container"]/ul/li[1]/a'
  text_field :search, :xpath=> '//*[@id="search-field"]'
  link :btnSearch, :xpath => '//*[@id="btnSearch"]'


  def clickCategory
    self.itemMenuCookware?
    self.itemMenuCookware

  end

  def searchValue value
    self.search
    self.search= value.to_s
    self.btnSearch
  end

end


