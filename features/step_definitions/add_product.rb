Given(/^i stay on the ecommerce page "([^"]*)"\.$/) do |url|
  visit IndexPage
end


Given(/^access category "([^"]*)" in menu\.$/) do |category|
  on(IndexPage).clickCategory
end

Given(/^access subcategory "([^"]*)" in menu\.$/) do |subcategory|
  on(CategoryPage).clickSubCategory
end

Given(/^click on product "([^"]*)"\.$/) do |product|
  on(SubCategoryPage).clickOnProduct
end

Then(/^Product page shows Add to cart button\.$/) do
  on(ProductPage).assertProductPageShowAddToCardButton
end

Then(/^Add product "([^"]*)" to cart\.$/) do |product|
  on(ProductPage).addProductToCard
end

When(/^click on Add to cart button, add to cart overlay appears\.$/) do
  on(CartOverlayPage).assertCartOverlayPageExists
end

Then(/^The product "([^"]*)" you added to cart should be on shopping cart page\.$/) do |product|
  on(CartOverlayPage).assertProductOnCart
end

When(/^Checkout button is on the add to cart overlay\.$/) do
  on(CartOverlayPage).assertCheckoutButtonIsPresented
end

When(/^click on Checkout button\.$/) do
  on(CartOverlayPage).clickCheckoutButton
end

Then(/^shopping cart page is shown\.$/) do
  on(CartPage).assertPageIsCartPage
end
