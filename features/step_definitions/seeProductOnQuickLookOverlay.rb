Given(/^I search for a product "([^"]*)" using input on the top right area,$/) do |product|
  on(IndexPage).searchValue product
end

Then(/^Search field takes to the results page$/) do
  on(ResultSearchPage).assertIsSearchResultPage
end

Then(/^Hover the product's image display the quick look button$/) do
  on(ResultSearchPage).scrollToQuickView
end

Then(/^Click the quick look button show the product overlay$/) do
   $name = on(ResultSearchPage).storeProductNameClicked
   #puts $name
  $price = on(ResultSearchPage).storeProductPriceClicked
   #puts $price
   on(ResultSearchPage).clickQuickOverview

end

Then(/^The product clicked should have the same name and price as the product in the overlay$/) do
  on(CartOverlayPage).assertNameAndPriceInCartOverlay $name, $price
end

